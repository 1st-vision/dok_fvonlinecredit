.. |label| replace:: Online Guthaben
.. |snippet| replace:: FvOnlineCredit
.. |Author| replace:: 1st Vision GmbH
.. |minVersion| replace:: 5.6.0
.. |maxVersion| replace:: 5.6.X
.. |version| replace:: 1.0.0
.. |php| replace:: 7.2


|label|
============

.. sectnum::

.. contents:: Inhaltsverzeichnis



Überblick
---------
:Author: |Author|
:PHP: |php|
:Kürzel: |snippet|
:getestet für Shopware-Version: |minVersion| bis |maxVersion|
:Version: |version|

Beschreibung
------------
Das Modul bietet den Kunden einen Online-Bonus in Form von Guthaben; es ist möglich Guthaben zu Sammeln und damit zu bezahlen.

Frontend
--------
Guthaben sammeln
Je nach dem was im Mapping definiert wurde, wird dem Kunden Guthaben auf sein Konto gutgeschrieben.

:Achtung: Das Guthaben errechnet sich aus dem Faktor und Nettopreisen von allen rabattfähigen Artikel im Warenkorb (z.B. 3,45% von allen Artikeln im Warenkorb mit dem Attribut „Artikel-Discount“ auf „ja“).

.. image:: FvOnlineCredit4.jpg

Anzeige Guthaben im Frontend:
Anzeige als Widget (steht somit überall zur Verfügung): 

.. image:: FvOnlineCredit5.jpg

wenn der Kunde nicht eingeloggt ist.

.. image:: FvOnlineCredit6.jpg

Anzeige im Account:

.. image:: FvOnlineCredit7.jpg

Guthaben einlösen:
Wenn der Kunde Guthaben besitzt dann wird im „Checkout“ ein Eingabefeld angezeigt, mit dem er sein Guthaben einsetzen kann. 
Negative Werte sind hier nicht möglich. Brutto/Netto wird natürlich berücksichtigt. Das Löschen vom Guthaben geht auch. Man kann Guthaben beliebig einsetzen bis man 0 Euro erreicht.

.. image:: FvOnlineCredit8.jpg

.. image:: FvOnlineCredit9.jpg

Nach der Bestellung wird der Kunde informiert wie der Guthaben-Stand ist und was ihm gutgeschrieben wurde. Die Anzeige ist dynamisch und richtet sich danach ob Guthaben eingesetzt worden ist, usw.

.. image:: FvOnlineCredit10.jpg



Backend
-------

.. image:: FvOnlineCredit1.jpg

:Definition Artikel-Attribut für die Rabattfähigkeit (ja/nein Feld)
(bei Kauf von diesen Artikeln bekommt der User ein Guthaben)
:Definition User-Attribut für die Speicherung vom Guthaben
:Guthabenabzug bei der Versandkostenberechnung berücksichtigen (Grenze "frei ab")

.. image:: FvOnlineCredit2.jpg

.. image:: FvOnlineCredit3.jpg

Mapping Online-Guthaben

:Es gibt eine kleine Administration, in der man ein Mapping fürs Guthaben anlegen muss. Berücksichtigt wird die Zahlungsart, die Kundengruppe und den Shop; hier kann man einen individuellen Faktor in Prozent angeben. D.h. hier wird definiert wie viel Prozent an Guthaben ein „Shopkunde“ bei der Zahlung mit „Vorkasse“ im „deutschen“ Shop erhält.


technische Beschreibung
------------------------
Speicherung der Werte bei der Bestellung
__________________

Folgende Felder werden in „s_order_attributes“ gespeichert:
:fv_online_credit: Guthaben, das mit dieser Bestellung gutgeschrieben wurde
:fv_online_credit_used: Eingesetztes Guthaben in dieser Bestellung

.. image:: FvOnlineCredit11.jpg


ACHTUNG ACHTUNG ACHTUNG 😊:
Man kann natürlich soviel Guthaben einsetzen bis die Gesamtsumme 0 ist. Das ist kein Problem, ABER… Manche Zahlungsmethoden (wie z.B. PayPal) machen natürlich an der Stelle Probleme, Hier empfehlen wir ein spezielles Plugin.
Das Modul legt eine neue Zahlungsmethode an (sie heißt „Kostenlos“), die automatisch anspringt, wenn der Betrag 0 Euro ist. Nach der Bezahlung verschwindet sie wieder.



Modifizierte Template-Dateien
-----------------------------
:/account/:
:/checkout/:



